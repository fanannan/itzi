#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import sys
import traceback
import zipfile
import numpy as np
import scipy.ndimage.interpolation as sni
import scipy.ndimage as ndimage

import PIL.Image
#
import nowcast.countrywide.run as nc
import nowcast.common.log as log
import nowcast.common.constant as const
#from . import dem_reader
import nowcast.map.dem.dem_reader as dem_reader



def show_error():
    info = sys.exc_info()
    tbinfo = traceback.format_tb(info[2])
    for tbi in tbinfo:
        print(tbi)
    print('  %s'%str(info[1]))
    print('\n'.rjust(80, '='))


def is_original():
    return os.environ[const.MODIFIED_ITZI] == '0'

def is_compatible():
    return os.environ[const.MODIFIED_ITZI] in ['1']

def is_replaced():
    return os.environ[const.MODIFIED_ITZI] in ['2']

# ネットワークマウント
# def mount_nowcast_data_path():
#     if not os.path.exists(const.MOUNT_CHECK_PATH):
#         if os.path.exists(const.BASH_PATH):
#             os.system(const.BASH_PATH+' '+const.MOUNTER_SCRIPT_PATH)
#         else:
#             raise Exception('bash is not found')


def read_5m_mesh(dem_main_id, dem_sub_id, dem_type, path, dem10=None):
    assert dem_type in ['5A', '5B']
    kind = dem_type[1]
    (Y_SIZE_ORIGINAL, X_SIZE_ORIGINAL) = (150, 225)
    (Y_SIZE, X_SIZE) = (Y_SIZE_ORIGINAL*10, X_SIZE_ORIGINAL*10)
    if dem10 is None:
        arr = np.zeros((Y_SIZE, X_SIZE))
        arr[:, :] = np.nan
    else:
        arr = dem10
    #print('reading mesh')
    for ns in range(10):
        for we in range(10):
            file_path = os.path.join(path, #'DEM5{}/'.format(kind),
                                     'FG-GML-{0:04d}-{1:02d}-{2:01d}{3:01d}-DEM5{4}-20130702.xml'.format(
                                         dem_main_id, dem_sub_id, ns, we, kind))
            if os.path.exists(file_path):
                try:
                    log.logger.info('loading 5m mesh file: {0}'.format(file_path))
                    part = dem_reader.read_map_file(file_path)
                    y = (9-ns)*Y_SIZE_ORIGINAL
                    x = we*X_SIZE_ORIGINAL
                    #print(part[:5], y, y+Y_SIZE_ORIGINAL, x, x+X_SIZE_ORIGINAL)
                    dem5m = part[6]
                    dem5m[dem5m<-100] = np.nan
                    #arr[y:y+Y_SIZE_ORIGINAL, x:x+X_SIZE_ORIGINAL] = dem5m
                    for yy in xrange(0, Y_SIZE_ORIGINAL):
                        for xx in xrange(0, X_SIZE_ORIGINAL):
                            #print(xx, yy)
                            v = dem5m[yy, xx]
                            if not np.isnan(v):
                                arr[y+yy, x+xx] = v
                except:
                    show_error()
                    exit()
                    pass
            else:
                log.logger.warn('DEM file not found: {0}'.format(file_path))
    return arr


def melt_5m(dem_main_id, dem_sub_id, dem_type, dem_path):
    assert dem_type in ['5A', '5B']
    kind = dem_type[1]
    zip_file_path = os.path.join(dem_path, 'DEM5{}/'.format(kind))+\
                    'FG-GML-{0:04d}-{1:02d}-DEM5{2}.zip'.format(dem_main_id, dem_sub_id, kind)
    if not os.path.exists(zip_file_path):
        raise Exception(zip_file_path+' is not found')
    log.logger.info('retrieving 5m meshfiles: {0}'.format(zip_file_path))
    with zipfile.ZipFile(zip_file_path, 'r') as zf:
        file_path = None
        for f in zf.infolist():
            if f.filename[-4:] == '.xml':
                file_path = os.path.join(const.TEMP_PATH, f.filename)
                break
        if file_path is None:
            raise Exception(file_path+' is not found in '+zip_file_path)
        if not os.path.exists(file_path):
            zf.extractall(const.TEMP_PATH)


def get_synth_5m(dem_id, dem_sub_id, dem_type, dem_path, dem10=None):
    if dem10 is None:
        try:
            dem10_info = dem_reader.read_map(dem_id, dem_sub_id, os.path.join(dem_path, const.DEM10A), '10A')
        except:
            dem10_info = dem_reader.read_map(dem_id, dem_sub_id, os.path.join(dem_path, const.DEM10B), '10B')
        dem10 = dem10_info[6]
    dem10_doubled = sni.zoom(dem10, 2)
    melt_5m(dem_id, dem_sub_id, dem_type, dem_path)
    dem5 = read_5m_mesh(dem_id, dem_sub_id, dem_type, const.TEMP_PATH, dem10_doubled)
    array = dem5 #np.fmax(dem10_doubled, dem5)
    #print(dem5.shape)
    #exit()
    return array


def get_synth_5m_D(dem_id, dem_sub_id, dem_id_d, dem_sub_id_d, dem_type, dem_path, dem10):
    u = get_synth_5m(dem_id, dem_sub_id, dem_type, dem_path, dem10)
    d = get_synth_5m(dem_id_d, dem_sub_id_d, dem_type, dem_path, None)
    array = np.concatenate((u, d), axis=0)
    return array


def get_dem(dem_info):
    #
    dem_id = dem_info['DEM_ID']
    dem_path = dem_info['DEM_PATH']
    dem_sub_id = dem_info['DEM_SUB_ID']
    dem_type = dem_info['DEM_TYPE']
    # 10m メッシュ地図取得
    if dem_type in ['10B']:
        dem10_info = dem_reader.read_map(dem_id, dem_sub_id, os.path.join(dem_path, const.DEM10B), '10B')
    else:
        try:
            dem10_info = dem_reader.read_map(dem_id, dem_sub_id, os.path.join(dem_path, const.DEM10A), '10A')
        except:
            dem10_info = dem_reader.read_map(dem_id, dem_sub_id, os.path.join(dem_path, const.DEM10B), '10B')
    (lcy, lcx, ucy, ucx, starty, startx, dem10,) = dem10_info
    # 5m
    if dem_type in ['5A', '5B']:
        array = get_synth_5m(dem_id, dem_sub_id, dem_type, dem_path, dem10)
    elif dem_type == '5A-D':
        dem_id_d = dem_info['DEM_ID_D']
        dem_sub_id_d = dem_info['DEM_SUB_ID_D']
        array = get_synth_5m_D(dem_id, dem_sub_id,
                               dem_id_d, dem_sub_id_d, dem_type, dem_path, dem10)
        raise
    elif dem_type in ['10A', '10B']:
        array = dem10
    else:
        raise Exception()
    return (lcy, lcx, ucy, ucx, starty, startx, array,)

# def get_raster_map(rast_name, sim_param):
#     size = sim_param['SIZE']
#     dem_id = sim_param['DEM_ID']
#     dem_sub_id = sim_param['DEM_SUB_ID']
#     dem_type = sim_param['DEM_TYPE']
#     if rast_name == 'elev_lid792_5m@PERMANENT':
#         dem10_info = dem_reader.read_map(dem_id, dem_sub_id, const.DEM10B_PATH)
#         dem10 = dem10_info[6]
#         if dem_type == '5A':
#             array = get_synth_5m(dem_id, dem_sub_id, dem10)
#         elif dem_type == '5A-D':
#             dem_id_d = sim_param['DEM_ID_D']
#             dem_sub_id_d = sim_param['DEM_SUB_ID_D']
#             array = get_synth_5m_D(dem_id, dem_sub_id,
#                                    dem_id_d, dem_sub_id_d, dem10)
#         else:
#             array = dem10
#         log.logger.info('DEM data: {0}'.format(array))
#     elif rast_name=='rain@PERMANENT':
#         array = np.zeros(size)+const.RAINFALL
#     elif rast_name=='n@PERMANENT':
#         array = np.zeros(size)+0.05
#     else:
#         array = np.zeros(size)
#     return array


def get_raster_map(rast_name, sim_param):
    size = sim_param['SIZE']
    if rast_name == 'elev_lid792_5m@PERMANENT':
        array = sim_param['DEM']
        log.logger.info('DEM data: {0}'.format(array))
    elif rast_name=='rain@PERMANENT':
        r = sim_param['CONSTANT_RAINFALL']
        array = np.zeros(size)+(0.0 if r == False else r)
    elif rast_name=='n@PERMANENT':
        array = np.zeros(size)
    else:
        array = np.zeros(size)
    return array


def read_nowcast(fs, sim_time, sim_param):
    r = sim_param['CONSTANT_RAINFALL']
    if r != False:
        return np.zeros((fs.shape[0], fs.shape[1]), dtype=np.float32)+r
    # ナウキャストデータ取得
    log.logger.info('reading nowcast data at {0}'.format(sim_time))
    rf = nc.get_view(sim_param['NOWCAST_PATH'],
                     sim_param['NORTH_Y'],
                     sim_param['EAST_X'],
                     sim_param['SOUTH_Y'],
                     sim_param['WEST_X'],
                     sim_time,
                     sim_param['CACHE_PATH'])
    log.logger.info('expanding nowcast data'.format())
    if rf is None:
        rf = np.zeros((fs.shape[0], fs.shape[1]))  # mm
    # print(fs.shape)
    # print(rf.shape)
    ncd = ndimage.zoom(rf, np.array([float(fs.shape[0])/float(rf.shape[0]),
                                     float(fs.shape[1])/float(rf.shape[1])]))
    return np.array(ncd*1.0, dtype=np.float32)


# def imagify(arr, rast_name, sim_param):
#     def scale(arr):
#         top = np.nanmax(arr)
#         bottom = np.nanmin(arr)
#         return np.uint8(255*(arr-bottom)/(top-bottom))
#     #values = filter(lambda x: not np.isnan(x), arr.flat)
#     if rast_name.find('_h_') > 0 or rast_name.find('_v_') > 0:
#         arr = np.log(arr)
#     thread = 100.0
#     bottom = np.nanmin(arr)
#     top = np.nanmax(arr)
#     log.logger.info('drawing {0}: {1}/{2}'.format(rast_name, bottom, top))
#     positives = np.maximum(arr, np.zeros_like(arr))
#     shallows = np.maximum(np.minimum(positives, np.zeros_like(positives)+thread), np.zeros_like(positives))
#     negatives = np.minimum(arr, np.zeros_like(arr))
#     im_positives = PIL.Image.fromarray(scale(positives))
#     im_shallows = PIL.Image.fromarray(scale(shallows))
#     im_negatives = PIL.Image.fromarray(scale(negatives))
#     img = PIL.Image.merge('RGB', (im_positives, im_shallows, im_negatives))
#     dem_id = sim_param['DEM_ID']
#     dem_sub_id = sim_param['DEM_SUB_ID']
#     dem_type = sim_param['DEM_TYPE']
#     img.save(os.path.join(const.TEMP_PATH, '{0:04d}_{1:02d}_{2}_DEM{3}.png'.format(
#             dem_id, dem_sub_id, rast_name, dem_type)))
#     return img

def imagify(arr, rast_name, sim_param):
    dem_id = sim_param['DEM_ID']
    dem_sub_id = sim_param['DEM_SUB_ID']
    dem_type = sim_param['DEM_TYPE']
    if rast_name.find('_h_') > 0:
        thresh1 = np.log(100.0)
        thresh2 = np.log(1000.0)
        thresh3 = np.log(10000.0)
        arr2 = np.log(arr)
    elif rast_name.find('_v_') > 0:
        thresh1 = np.log(10.0)
        thresh2 = np.log(100.0)
        thresh3 = np.log(1000.0)
        arr2 = np.log(arr)
    elif rast_name.find('_q') > 0:
        thresh1 = np.log(10.0)
        thresh2 = np.log(100.0)
        thresh3 = np.log(1000.0)
        arr2 = np.log(arr)
    elif rast_name.find('_wse') > 0 or rast_name.find('elev')>-1:
        thresh1 = 250.0
        thresh2 = 1000.0
        thresh3 = 4000.0
        arr2 = arr
    elif rast_name.find('_rainfall') > 0:
        thresh1 = np.log(10.0)
        thresh2 = np.log(100.0)
        thresh3 = np.log(1000.0)
        arr2 = np.log(arr)
    else:
        print('skipping drawing: '+rast_name)
        return
        raise Exception(rast_name)
    #
    def scale(arr, upper, lower):
        z = np.zeros_like(arr)
        u = np.zeros_like(arr)+upper
        return np.uint8(255*(np.minimum(np.maximum(arr, z), u)-lower)/(upper-lower))
    #
    log.logger.info('drawing {0}: {1}/{2}'.format(rast_name, np.nanmin(arr), np.nanmax(arr)))
    # im1 = PIL.Image.fromarray(scale(arr2, 0, thresh1))
    # im2 = PIL.Image.fromarray(scale(arr2, thresh1, thresh2))
    # im3 = PIL.Image.fromarray(scale(arr2, thresh2, thresh3))
    # img = PIL.Image.merge('RGB', (im3, im2, im1))
    import matplotlib.pyplot as plt
    from matplotlib import cm
    #arr3 = np.minimum(np.maximum(arr2/thresh3, np.zeros_like(arr)), np.zeros_like(arr))
    #arr3 = np.maximum(arr2/thresh3, np.zeros_like(arr))
    arr3 = arr2/thresh3
    arr3[arr3 <= 0.0] = np.nan
    arr3 = np.ma.array(arr3, mask=np.isnan(arr3))
    cmap = plt.get_cmap('Paired')
    img = PIL.Image.fromarray(np.uint8(cmap(arr3)*255))
    #img = PIL.Image.fromarray(np.uint8(cm.gist_earth(myarray)*255))
    file_body = os.path.join(const.TEMP_PATH, '{0:04d}_{1:02d}_{2}_DEM{3}'.format(
            dem_id, dem_sub_id, rast_name, dem_type))
    img.save(file_body+'.png')
    np.save(file_body+'.npy', arr)
    return img
