#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import zipfile
import numpy as np
import scipy.ndimage.interpolation as sni
from nz import const, log, dem_reader


def is_original():
    return os.environ[const.MODIFIED_ITZI] == '0'

def is_compatible():
    return os.environ[const.MODIFIED_ITZI] in ['1']

def is_replaced():
    return os.environ[const.MODIFIED_ITZI] in ['2']

# ネットワークマウント
def mount_nowcast_data_path():
    if not os.path.exists(const.MOUNT_CHECK_PATH):
        if os.path.exists(const.BASH_PATH):
            os.system(const.BASH_PATH+' '+const.MOUNTER_SCRIPT_PATH)
        else:
            raise Exception('bash is not found')

def read_5m_mesh(path, kind, dem_main_id, dem_sub_id, dem10=None):
    (Y_SIZE_ORIGINAL, X_SIZE_ORIGINAL) = (150, 225)
    (Y_SIZE, X_SIZE) = (Y_SIZE_ORIGINAL*10, X_SIZE_ORIGINAL*10)
    if dem10 is None:
        arr = np.zeros((Y_SIZE, X_SIZE))
        arr[:, :] = np.nan
    else:
        arr = dem10
    for ns in range(10):
        for we in range(10):
            file_path = os.path.join(path,
                                     'FG-GML-{0:04d}-{1:02d}-{2:01d}{3:01d}-DEM5{4}-20130702.xml'.format(
                                         dem_main_id, dem_sub_id, ns, we, kind))
            if os.path.exists(file_path):
                try:
                    print(file_path)
                    part = dem_reader.read_map_file(file_path)
                    y = (9-ns)*Y_SIZE_ORIGINAL
                    x = we*X_SIZE_ORIGINAL
                    print(part[:5], y, x)
                    m = part[6]
                    m[m<-100] = np.nan
                    arr[y:y+Y_SIZE_ORIGINAL, x:x+X_SIZE_ORIGINAL] = m
                except:
                    print('loading failed at '+file_path)
                    pass
    return arr


def melt_5m(dem_path, kind, dem_main_id, dem_sub_id):
    zip_file_path = dem_path+'FG-GML-{0:04d}-{1:02d}-DEM5{2}.zip'.format(dem_main_id, dem_sub_id, kind)
    if not os.path.exists(zip_file_path):
        raise Exception(zip_file_path+' is not found')
    with zipfile.ZipFile(zip_file_path, 'r') as zf:
        file_path = None
        for f in zf.infolist():
            if f.filename[-4:]=='.xml':
                file_path = os.path.join(const.TEMP_PATH, f.filename)
                break
        if file_path is None:
            raise Exception(file_path+' is not found in '+zip_file_path)
        if not os.path.exists(file_path):
            zf.extractall(const.TEMP_PATH)

def get_synth_5m(dem_id, dem_sub_id, dem10=None):
    if dem10 is None:
        dem10_info = dem_reader.read_map(dem_id, dem_sub_id, const.DEM_PATH)
        dem10 = dem10_info[6]
    dem10_doubled = sni.zoom(dem10, 2)
    melt_5m(const.DEM5_PATH, 'A', dem_id, dem_sub_id)
    dem5 = read_5m_mesh(const.TEMP_PATH, 'A', dem_id, dem_sub_id, dem10_doubled)
    array = dem5 #np.fmax(dem10_doubled, dem5)
    return array

def get_synth_5m_D(dem_id, dem_sub_id, dem_id_d, dem_sub_id_d, dem10):
    u = get_synth_5m(dem_id, dem_sub_id, dem10)
    d = get_synth_5m(dem_id_d, dem_sub_id_d, None)
    array = np.concatenate((u, d), axis=0)
    return array

def get_raster_map(rast_name):
    if const.DEM_5m:
        size = 1500, 2250
    elif const.DEM_5m_D:
        size = 1500*2, 2250
    else:
        size = 750, 1125
    if rast_name == 'elev_lid792_5m@PERMANENT':
        dem10_info = dem_reader.read_map(const.DEM_ID, const.DEM_SUB_ID, const.DEM_PATH)
        dem10 = dem10_info[6]
        if const.DEM_5m:
            array = get_synth_5m(const.DEM_ID, const.DEM_SUB_ID, dem10)
        elif const.DEM_5m_D:
            array = get_synth_5m_D(const.DEM_ID, const.DEM_SUB_ID, const.DEM_ID_D, const.DEM_SUB_ID_D, dem10)
        else:
            array = dem10
        log.logger.info('DEM data: {0}'.format(array))
    elif rast_name=='rain@PERMANENT':
        array = np.zeros(size)+const.RAINFALL
    elif rast_name=='n@PERMANENT':
        array = np.zeros(size)+0.05
    else:
        array = np.zeros(size)
    return array
