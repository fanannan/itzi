#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import division
import numpy as np
from datetime import datetime, timedelta
from collections import namedtuple
import os

from .. import messenger as msgr

#import grass.script as grass
import grass.temporal as tgis
from grass.pygrass import raster
import grass.pygrass.utils as gutils
from grass.exceptions import FatalError, CalledModuleError

from .. import gis
import nowcast.common.log as log
from . import utils

ORIGINAL = utils.is_original()
COMPATIBLE = utils.is_compatible()
REPLACED = utils.is_replaced()
assert ORIGINAL == False
if REPLACED:
    import nowcast.common.constant as const
    from region import Region
    from itzi_src.itzi.nz import utils as iu
else:
    from grass.pygrass.gis.region import Region


class Igis(gis.Igis):

    def __init__(self, start_time, end_time, dtype, mkeys, sim_param):
        super(Igis, self).__init__(start_time, end_time, dtype, mkeys, sim_param)
        self.sim_param = sim_param
        log.logger.info('start time: {0}'.format(self.start_time))
        log.logger.info('end time: {0}'.format(self.end_time))
        log.logger.info('dtype: {0}'.format(self.dtype))
        # Region: libgis.G_get_default_window(self.byref())
        # int G_get_default_window() read the default region
        # Reads the default region for the location into region.
        log.logger.info('xr (region.cols): {0}'.format(self.xr))
        log.logger.info('yr (region.rows): {0}'.format(self.yr))
        log.logger.info('dx (region.ewres): {0}'.format(self.dx))
        log.logger.info('dy (region.nsres): {0}'.format(self.dy))
        log.logger.info('overwrite: {0}'.format(self.overwrite))
        log.logger.info('mapset: {0}'.format(self.mapset))
        log.logger.info('gutills mapset: {0}'.format(gutils.getenv('MAPSET')))
        log.logger.info('maps: {0}'.format(self.maps))
        log.logger.info('cols: {0}'.format(self.cols))
        log.logger.info('MapData: {0}'.format(self.MapData))
        log.logger.info('rules_h: {0}'.format(self.rules_h))
        log.logger.info('rules_v: {0}'.format(self.rules_v))
        log.logger.info('rules_def: {0}'.format(self.rules_def))
        log.logger.info('sim_param: {0}'.format(self.sim_param))

    def read(self, map_names):
        """Read maps names from GIS
        take as input map_names, a dictionary of maps/STDS names
        for each entry in map_names:
            if the name is empty or None, store None
            if a strds, load all maps in the instance's time extend,
                store them as a list
            if a single map, set the start and end time to fit simulation.
                store it in a list for consistency
        each map is stored as a MapData namedtuple
        store result in instance's dictionary
        """
        for k, map_name in map_names.iteritems():
            if not map_name:
                map_list = None
                continue
            elif self.name_is_stds(self.format_id(map_name)):
                strds_id = self.format_id(map_name)
                if not self.stds_temporal_sanity(strds_id):
                    msgr.fatal(u"{}: inadequate temporal format"
                               u"".format(map_name))
                map_list = self.raster_list_from_strds(strds_id)
            elif self.name_is_map(self.format_id(map_name)):
                map_list = [self.MapData(id=self.format_id(map_name),
                                         start_time=self.start_time,
                                         end_time=self.end_time)]
            else:
                msgr.fatal(u"{} not found!".format(map_name))
            log.logger.info('map list: {0} / {1}'.format(k, map_list))
            self.maps[k] = map_list
        return self

    def stds_temporal_sanity(self, stds_id):
        """Make the following check on the given stds:
        - Topology is valid
        - No gap
        - Cover all simulation time
        return True if all the above is True, False otherwise
        """
        out = True
        stds = tgis.open_stds.open_old_stds(stds_id, 'strds')
        log.logger.info('stds id:{0} / {1}'.format(stds_id, stds))
        # valid topology
        if not stds.check_temporal_topology():
            out = False
            msgr.warning(u"{}: invalid topology".format(stds_id))
        # no gap
        if stds.count_gaps()!=0:
            out = False
            msgr.warning(u"{}: gaps found".format(stds_id))
        # cover all simulation time
        sim_start, sim_end = self.get_sim_extend_in_stds_unit(stds)
        stds_start, stds_end = stds.get_temporal_extent_as_tuple()
        if stds_start>sim_start:
            out = False
            msgr.warning(u"{}: starts after simulation".format(stds_id))
        if stds_end<sim_end:
            out = False
            msgr.warning(u"{}: ends before simulation".format(stds_id))
        return out

    def raster_list_from_strds(self, strds_name):
        """Return a list of maps from a given strds
        for all the simulation duration
        Each map data is stored as a MapData namedtuple
        """
        assert isinstance(strds_name, basestring), "expect a string"

        # transform simulation start and end time in strds unit
        strds = tgis.open_stds.open_old_stds(strds_name, 'strds')
        sim_start, sim_end = self.get_sim_extend_in_stds_unit(strds)

        # retrieve data from DB
        where = "start_time <= '{e}' AND end_time >= '{s}'".format(
            e=str(sim_end), s=str(sim_start))
        maplist = strds.get_registered_maps(columns=','.join(self.cols),
                                            where=where,
                                            order='start_time')
        # check if every map exist
        maps_not_found = [m[0] for m in maplist if not self.name_is_map(m[0])]
        if any(maps_not_found):
            err_msg = u"STRDS <{}>: Can't find following maps: {}"
            str_lst = ','.join(maps_not_found)
            msgr.fatal(err_msg.format(strds_name, str_lst))
        # change time data to datetime format
        if strds.get_temporal_type()=='relative':
            rel_unit = strds.get_relative_time_unit().encode('ascii', 'ignore')
            maplist = [(i[0], self.to_datetime(rel_unit, i[1]),
                        self.to_datetime(rel_unit, i[2])) for i in maplist]
        return [self.MapData(*i) for i in maplist]

    def read_raster_map(self, rast_name):
        """Read a GRASS raster and return a numpy array
        """
        log.logger.info('reading raster map: {0}'.format(rast_name))
        if REPLACED:
            array = utils.get_raster_map(rast_name, self.sim_param)
        else:
            array = super(Igis, self).read_raster_map(rast_name)
        values = filter(lambda x: not np.isnan(x), array.flat)
        log.logger.info('raster map yr:{0} xr:{1}'.format(array.shape[0], array.shape[1]))
        log.logger.info('num valid cells: {0}/{1}'.format(
            len(values),
            array.shape[0]*array.shape[1]))
        log.logger.info('max, ave, min: {0}, {1}, {2}'.format(
            max(values), np.array(values).mean(), min(values)))
        if not ORIGINAL:
            iu.imagify(array, rast_name, self.sim_param)
        return array

    def write_raster_map(self, arr, rast_name, mkey):
        """Take a numpy array and write it to GRASS DB
        """
        log.logger.info('writing raster map {0}: {1} {2}'.format(mkey, arr.shape, rast_name))
        if not ORIGINAL:
            iu.imagify(arr, rast_name, self.sim_param)
        return super(Igis, self).write_raster_map(arr, rast_name, mkey)

    def get_array(self, mkey, sim_time):
        """take a given map key and simulation time
        return a numpy array associated with its start and end time
        if no map is found, return None instead of an array
        and the start_time and end_time of the simulation
        """
        log.logger.info('getting raster map for {0}: {1}'.format(sim_time, self.maps[mkey]))
        return super(Igis, self).get_array(mkey, sim_time)

    def register_maps_in_strds(self, mkey, strds_name, map_list, t_type):
        '''Register given maps
        '''
        log.logger.info('registering map in strds {0}/{3}: {1}, {2}'.format(
            mkey, strds_name, map_list, t_type))
        return super(Igis, self).register_maps_in_strds(mkey, strds_name, map_list, t_type)


log.logger.info('Dummy GIS module is loaded')
