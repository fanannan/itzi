#!/usr/bin/env python
# -*- coding: utf-8 -*-

from . import utils
REPLACED = utils.is_replaced()


class Region():
    def __init__(self, dem_type):
        self.dem_type = dem_type


    @property
    def cols(self):
        if REPLACED:
            if self.dem_type in ['5A', '5B', '5A-D', '5B-D']:
                return 2250
            elif self.dem_type in ['5A-R', '5B-R']:
                return 2250*2
            elif self.dem_type in ['10A', '10B']:
                return 1125
            #print(self.dem_type)
            raise Exception()
        else:
            return 140

    @property
    def rows(self):
        if REPLACED:
            if self.dem_type in ['5A', '5B', '5A-R', '5B-R']:
                return 1500
            elif self.dem_type in ['5A-D', '5B-D']:
                return 1500*2
            elif self.dem_type in ['10A', '10B']:
                return 750
            raise Exception()
        else:
            return 150

    @property
    def ewres(self):
        if REPLACED:
            if self.dem_type in ['5A', '5B', '5B-R', '5B-D']:
                return 5.
            elif self.dem_type in ['10A', '10B']:
                return 10.0
            raise Exception()
        else:
            return 5.

    @property
    def nsres(self):
        return self.ewres

